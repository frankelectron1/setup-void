#!/bin/bash
SCRIPT_PATH=$(dirname $(realpath $0))

ODROID_MODEL=`grep "MODEL" /opt/setup-void/install_parameter.txt | cut -d"=" -f2`

touch /etc/xbps.d/void-repo-frankelectron.conf
echo " repository=https://gitlab.com/frankelectron1/void-repo-frankelectron/-/raw/main/aarch64" | tee -a /etc/xbps.d/void-repo-frankelectron.conf

touch /etc/xbps.d/ignore.conf
echo "ignorepkg=linux-firmware-amd" | tee -a /etc/xbps.d/ignore.conf
echo "ignorepkg=linux-firmware-nvidia" | tee -a /etc/xbps.d/ignore.conf

echo 'echo "ondemand" | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor' | tee -a /etc/rc.local

sed -i "/# %wheel ALL=(ALL:ALL) ALL/s/^# //" /etc/sudoers

xbps-install -Suy xbps
xbps-install -Suy
xbps-install -Sy base-system 
xbps-remove -y base-container-full

xbps-install -Sy dracut nano chrony linux linux-headers
xbps-pkgdb -m repolock linux linux-headers linux6.3 linux6.3-headers

echo -e "Enter Hostname for the System"
echo -e "Example:   void"
read -p "	: " HOST_NAME

echo "$HOST_NAME" | tee /etc/hostname

unset HOST_NAME

echo -e "Create a new User. Enter the Name for the new User"
echo -e "Example:   void"
read -p "	: " NEW_USER

useradd -m -G audio,cdrom,floppy,kvm,optical,storage,video,wheel,xbuilder $NEW_USER
echo "Enter Password for user $NEW_USER"
passwd $NEW_USER

echo "/bin/bash /opt/setup-void/setup-void.sh 	# setup-void" | tee -a /home/$NEW_USER/.bash_profile

unset NEW_USER

usermod -s /bin/bash root
echo "Enter Password for user root"
passwd root

ln -s $SCRIPT_PATH/90-kboot-installer /etc/kernel.d/post-install

xbps-remove -O
xbps-reconfigure -fa
rm /var/cache/xbps/*

ln -s /etc/sv/{sshd,dhcpcd,chronyd} /etc/runit/runsvdir/default/

rm /etc/kernel.d/post-install/90-kboot-installer

ln -s $SCRIPT_PATH/90-kboot /etc/kernel.d/post-install
ln -s $SCRIPT_PATH/90-repolock /etc/kernel.d/post-install
ln -s $SCRIPT_PATH/90-vkpurge /etc/kernel.d/pre-install
ln -s $SCRIPT_PATH/90-cleanup /etc/kernel.d/post-remove
ln -s $SCRIPT_PATH/setup-void.sh /usr/local/bin/setup-void

history -c

$(exit)

exit
