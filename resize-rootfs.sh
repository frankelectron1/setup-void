# /bin/bash

SCRIPT_PATH=/opt/setup-void
# dedect filesystem of /
FILESYSTEM=`findmnt / -o fstype -n`
# dedect device of /
ROOT_DEV_PART=`findmnt / -o source -n | cut -d "/" -f3`
ROOT_DEV=`lsblk -ndo pkname /dev/$ROOT_DEV_PART`
# dedect partition number of /
PART_N_ROOT=`echo "$ROOT_DEV_PART" | grep -Eo '[0-9]+$'`
NEW_ROOTFS_SIZE=`grep "NEW_ROOTFS_SIZE" /opt/setup-void/install_parameter.txt | cut -d"=" -f2`

# delete autostart
sed -i /"resize-rootfs.sh"/d /etc/rc.local | tee -a /etc/rc.local

touch /forcefsck

sleep 6

# resize partition
if [ -z $NEW_ROOTFS_SIZE ]; then
    echo ", +" | sfdisk --no-reread -N $PART_N_ROOT /dev/$ROOT_DEV
else
    echo ", $NEW_ROOTFS_SIZE" | sfdisk --no-reread -N $PART_N_ROOT /dev/$ROOT_DEV
fi

sleep 2

# kernel update partition
partx -u /dev/$ROOT_DEV

sleep 2

# resize filesystem
if [ $FILESYSTEM = "ext4" ]; then
    resize2fs /dev/$ROOT_DEV_PART
elif [ $FILESYSTEM = "btrfs" ]; then
    btrfs filesystem resize max /
elif [ $FILESYSTEM = "xfs" ]; then
    xfs_growfs -d /
fi

sleep 2

reboot
