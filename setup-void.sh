# /bin/bash
#set -e

GREEN=$(tput setaf 2)
NONE=$(tput sgr0)

SCRIPT_NAME=`basename "$0"`
SCRIPT_PATH=$(dirname $(realpath $0))
CONTENT_PATH=$SCRIPT_PATH

DO_REBOOT=0

SET_KBLAYOUT=""
SET_KBVARIANTA=""
SET_KBVARIANTB=""

PACKAGES_TO_INSTALL_F=""
SERVICES_TO_ENABLE_F=()
PACKAGES_TO_INSTALL=""
SERVICES_TO_ENABLE=()
DISPLAY_MANAGER=""
DESKTOP_TO_INSTALL=""

ODROID_MODEL="`sudo dmesg | grep Machine | cut -d "-" -f 2`"


show_head () {
	clear
	echo -e "\n${GREEN}Setup Void on Odroid${NONE} $1\n"
}

press_continue () {
	echo -e "\n	${GREEN}Press enter to continue... ${NONE}\n"
	read -n 1 -r -s -p $"	"
}


exit_script () {
	show_head " - Exit"
	
	if ! [ -z $DISPLAY_MANAGER ]; then
		enable_service $DISPLAY_MANAGER
	fi
	
	if [ $DO_REBOOT = 1 ]; then
		echo -e "\n	${GREEN}Reboot System${NONE}\n"
		sudo reboot
	else
		echo -e "\n${GREEN}	Exit${NONE}\n"
	fi
	exit
}


cp_directory () {
	if [ -d $1 ]; then
		if [ -d $2 ]; then
			sudo cp -ra $1 $2
			echo -e "\n${GREEN}	sudo cp $1 $2	${NONE}\n"
		else
			echo -e "\n	Target $2 doesn't exist\n"
			press_continue
		fi
	else
		echo -e "\n	$1 doesn't exist\n"
		press_continue
	fi
}


cp_file () {
	if [ -f $1 ]; then
		if [ -d $2 ]; then
			sudo cp -a $1 $2
			echo -e "\n${GREEN}	sudo cp $1 $2	${NONE}\n"
		else
			echo -e "\n	Target $2 doesn't exist\n"
			press_continue
		fi
	else 
		echo -e "\n	$1 doesn't exist\n"
		press_continue
	fi
}


toggle_var () {
	if [ "$1" = " " ]; then
		echo "*"
	else
		echo " "
	fi
}


enable_service () {
	if [ -d /etc/sv/$1 ]; then
		echo -e "\n${GREEN}	Enable Service $1.${NONE}\n"
		sudo ln -s /etc/sv/$1 /var/service/
	else
		echo -e "\n${GREEN}	Service $1 not found.${NONE}\n"
		sleep 2
	fi
}


update_setup_void () {
	show_head " - Update setup-void"
	echo -e "	\n${GREEN}Update and restart ${NONE}setup-void${GREEN}.${NONE}\n"
	read -p "	Continue? (y/n): " YN_CHOICE
	
	if [ "$YN_CHOICE" = "y" ]; then
		sudo git -C $SCRIPT_PATH pull https://gitlab.com/frankelectron1/setup-void.git

		if ! [ -f /usr/local/bin/setup-void ]; then
			sudo ln -s $SCRIPT_PATH/setup-void.sh /usr/local/bin/setup-void
		fi

		if ! [ -f /etc/kernel.d/post-install/90-kboot ]; then
			sudo ln -s $SCRIPT_PATH/90-kboot /etc/kernel.d/post-install/
		fi

		if ! [ -f /etc/kernel.d/post-install/90-repolock ]; then
			sudo ln -s $SCRIPT_PATH/90-repolock /etc/kernel.d/post-install/
		fi

		if ! [ -f /etc/kernel.d/post-remove/90-cleanup ]; then
			sudo ln -s $SCRIPT_PATH/90-cleanup /etc/kernel.d/post-remove/
		fi

		if ! [ -f /etc/xbps.d/void-repo-frankelectron.conf ]; then
			echo "repository=https://gitlab.com/frankelectron1/void-repo-frankelectron/-/raw/main/aarch64" | sudo tee -a /etc/xbps.d/void-repo-frankelectron.conf
		fi

		if ! [ -f /etc/xbps.d/ignore.conf ]; then
			sudo touch /etc/xbps.d/ignore.conf
			echo "ignorepkg=linux-firmware-amd" | sudo tee -a /etc/xbps.d/ignore.conf
			echo "ignorepkg=linux-firmware-nvidia" | sudo tee -a /etc/xbps.d/ignore.conf
		fi

		press_continue
		exec setup-void # $SCRIPT_PATH/$SCRIPT_NAME
	fi
	unset YN_CHOICE
}


update_system () {
	show_head " - Updating System"
	sleep 1
	sudo xbps-install -Suy xbps
	sudo xbps-install -Suy
}


reconfig_clean () {
	show_head " - Reconfigure and Cleanup"
	sleep 1
	sudo xbps-reconfigure -fa
	sudo xbps-remove -oO
}


delete_autostart () {
	echo -e "\n${GREEN}	Deleting autostart for this Script in ${NONE} ~/.bash_profile\n"
	sed -i /$SCRIPT_NAME/d ~/.bash_profile | tee -a ~/.bash_profile
}


set_xkb () {
	if [ -f /usr/share/X11/xorg.conf.d/40-libinput.conf ]; then
		LINE_N=`sudo grep -n '/usr/share/X11/xorg.conf.d/40-libinput.conf' -e 'libinput keyboard catchall' | cut -d":" -f1`
		LINE_N=$(( $LINE_N+4 ))
		if ! [ -z $SET_KBLAYOUT ]; then
			OPTION='Option "XkbLayout" "'$SET_KBLAYOUT'"'
			sudo sed -i.bak "${LINE_N}i\\	$OPTION\\" /usr/share/X11/xorg.conf.d/40-libinput.conf
			if ! [ -z $SET_KBVARIANTA ]; then
				VARIANT="$SET_KBVARIANTA"
				if ! [ -z $SET_KBVARIANTB ]; then
					VARIANT+=",$SET_KBVARIANTB"
				fi
				LINE_N=$(( $LINE_N+1 ))
				OPTION='Option "XkbVariant" "'$VARIANT'"'
				sudo sed -i.bak "${LINE_N}i\\	$OPTION\\" /usr/share/X11/xorg.conf.d/40-libinput.conf
				unset VARIANT
			fi
			unset OPTION
		fi
		unset LINE_N
	fi
}


setup_dm () {
	if [ "$DISPLAY_MANAGER" = "lightdm" ]; then
		echo "[greeter]" | sudo tee /etc/lightdm/lightdm-gtk-greeter.conf > /dev/null
		echo "theme-name = Breeze-Dark" | sudo tee -a /etc/lightdm/lightdm-gtk-greeter.conf > /dev/null
		echo "icon-theme-name = breeze-dark" | sudo tee -a /etc/lightdm/lightdm-gtk-greeter.conf > /dev/null
		echo "indicators = ~host;~spacer;~language;~session;~power;~clock;~layout;~a11y" | sudo tee -a /etc/lightdm/lightdm-gtk-greeter.conf > /dev/null
		echo "background = /usr/share/void-artwork/oldsplash.png" | sudo tee -a /etc/lightdm/lightdm-gtk-greeter.conf > /dev/null
		
	elif [ "$DISPLAY_MANAGER" = "lxdm" ]; then
		LINE_N=`sudo grep -n '/etc/lxdm/lxdm.conf' -e 'greeter=' | cut -d":" -f1`
		sudo sed -i -e ${LINE_N}c'greeter=/usr/libexec/lxdm-greeter-gtk' /etc/lxdm/lxdm.conf
		LINE_N=`sudo grep -n '/etc/lxdm/lxdm.conf' -e 'gtk_theme=' | cut -d":" -f1`
		sudo sed -i -e ${LINE_N}c'gtk_theme=Adwaita-dark' /etc/lxdm/lxdm.conf
		LINE_N=`sudo grep -n '/etc/lxdm/lxdm.conf' -e 'bg=' | cut -d":" -f1`
		sudo sed -i -e ${LINE_N}c'bg=/usr/share/void-artwork/oldsplash.png' /etc/lxdm/lxdm.conf
		LINE_N=`sudo grep -n '/etc/lxdm/lxdm.conf' -e 'theme=' | cut -d":" -f1`
		sudo sed -i -e ${LINE_N}c'theme=vdojo' /etc/lxdm/lxdm.conf
		LINE_N=`sudo grep -n '/usr/share/lxdm/themes/vdojo/greeter-gtk3.ui' -e 'image1' | cut -d":" -f1`
		LINE_N=$(( $LINE_N+1 ))
		sudo sed -i -e ${LINE_N}c\\'                    <property name="visible">False</property>'\\ /usr/share/lxdm/themes/vdojo/greeter-gtk3.ui
		unset LINE_N
	fi
}


setup_pipewire () {
	sudo ln -s /usr/share/applications/pipewire.desktop /etc/xdg/autostart/pipewire.desktop

	sudo mkdir -p /etc/pipewire/pipewire.conf.d
	sudo ln -s /usr/share/examples/wireplumber/10-wireplumber.conf /etc/pipewire/pipewire.conf.d/

	sudo ln -s /usr/share/examples/pipewire/20-pipewire-pulse.conf /etc/pipewire/pipewire.conf.d/
}


setup_amixer () { 	
 	if ! [ -z $ODROID_MODEL ]; then
		if [ "$ODROID_MODEL" = "N2" ] || [ "$ODROID_MODEL" = "N2Plus" ]; then
			amixer sset 'FRDDR_A SINK 1 SEL' 'OUT 1' || true
			amixer sset 'FRDDR_A SRC 1 EN' 'on' || true
			amixer sset 'TDMOUT_B SRC SEL' 'IN 0' || true
			amixer sset 'TOHDMITX I2S SRC' 'I2S B' || true
			amixer sset 'TOHDMITX' 'on' || true

			amixer sset 'FRDDR_B SINK 1 SEL' 'OUT 2' || true
			amixer sset 'FRDDR_B SRC 1 EN' 'on' || true
			amixer sset 'TDMOUT_C SRC SEL' 'IN 1' || true
			amixer sset 'TOACODEC SRC' 'I2S C' || true
			amixer sset 'TOACODEC OUT EN' 'on' || true
			amixer sset 'TOACODEC Lane Select' '0' || true
			amixer sset 'ACODEC' '255'

			amixer sset 'FRDDR_C SINK 1 SEL' 'OUT 3' || true
			amixer sset 'FRDDR_C SRC 1 EN' 'on' || true
			amixer sset 'SPDIFOUT SRC SEL' 'IN 2' || true

			sudo alsactl store
						
		elif [ "$ODROID_MODEL" = "C4" ] || [ "$ODROID_MODEL" = "HC4" ]; then
			amixer sset 'FRDDR_A SINK 1 SEL' 'OUT 1' || true
			amixer sset 'FRDDR_A SRC 1 EN' 'on' || true
			amixer sset 'TDMOUT_B SRC SEL' 'IN 0' || true
			amixer sset 'TOHDMITX I2S SRC' 'I2S B' || true
			amixer sset 'TOHDMITX' 'on' || true
			
			sudo alsactl store
		fi
	fi
}


change_passwd_root () {
	show_head " - Change password for root"
	echo -e "\n	${NONE}Password${GREEN} means your current password.${NONE}\n"
	echo -e "	${NONE}New password ${GREEN}means the new password for root${NONE}\n"
	sudo passwd root
	press_continue
}


change_passwd () {
	show_head " - Change password"
	echo -e "\n	Password${GREEN} means your current password.${NONE} \n"
	echo -e "	${NONE}New password ${GREEN}means the new password for root.${NONE}\n"
	read -p "	Continue? (y/n): " YN_CHOICE
	
	if [ "$YN_CHOICE" = "y" ]; then
		echo -e "\n	"
		read -p "	User? : " USER
		
		sudo passwd $USER
		press_continue
		unset USER
	else
		echo -e "\n${GREEN}	No changes.${NONE}\n"
		press_continue
	fi
	
	unset YN_CHOICE
}


create_new_user () {
	show_head " - Create new User"
	echo -e "\n${GREEN}	This will create a new User.${NONE}\n"
	read -p "	Continue? (y/n): " YN_CHOICE
	
	if [ "$YN_CHOICE" = "y" ]; then
		echo -e "\n${GREEN}	Name for new User. ! One Word and low characters only!${NONE}\n"
		read -p "	 : " USER
		sudo useradd -m -s /bin/bash -G audio,cdrom,floppy,kvm,optical,storage,video,wheel,xbuilder $USER
		sudo passwd $USER
		echo -e "\n${GREEN}	New User ${NONE}$USER${GREEN}. You can delete the User ${NONE}void${GREEN} by executing ${NONE} sudo userdel -r void${GREEN} as the new User.${NONE} \n"
		press_continue
		unset USER
	fi
	unset YN_CHOICE
}


set_lang () {
	show_head " - Language"
	echo -e "\n${GREEN}	Type the UTF-8 code for your Language. Leave it plank for no changes.
	If you don't know exactly you can look at ${NONE}/etc/default/libc-locales${NONE}.\n"
	echo -e "${GREEN}	Example: 	${NONE}de_DE.UTF-8\n"
	read -p " : " LANG_CHOICE
	
	if ! [ -z $LANG_CHOICE ] ; then
		if grep $LANG_CHOICE /etc/default/libc-locales; then
			sudo sed -i "/#$LANG_CHOICE\ UTF-8/s/^#//" /etc/default/libc-locales
			sudo sed -i -e 1c"LANG=$LANG_CHOICE" /etc/locale.conf
			sudo xbps-reconfigure -f glibc-locales
			echo -e "\n${GREEN}	Language is now ${NONE}$LANG_CHOICE\n"
			press_continue
		else
			echo -e "\n${GREEN}	This UTF-8 code doesn't exist in ${NONE}/etc/default/libc-locales"
		fi
	fi
	unset LANG_CHOICE
}


set_key_layout () {
	show_head " - Keyboard layout"
	echo -e "\n${GREEN}	Type the code for your Keyboard layout. Leave it plank for no changes.${NONE}\n"
	echo -e "${GREEN}	Example: 	${NONE}de\n"
	read -p "	 : " KEYLA
	
	if ! [ -z $KEYLA ]; then
		SET_KBLAYOUT="$KEYLA"
		echo -e "\n${GREEN}	You can set two variants.${NONE}\n"
		echo -e "${GREEN}	Example variant 1: 	${NONE}latin1\n"
		echo -e "${GREEN}	Example variant 2: 	${NONE}nodeadkeys\n"
		echo -e "\n${GREEN}	Type your Keyboard variant 1. Leave it plank for no changes.${NONE}\n"
		read -p "	 : " KEYVARA
		
		if ! [ -z $KEYVARA ]; then
			SET_KBVARIANTA="$KEYVARA"
			KEYLA+="-$KEYVARA"
			echo -e "\n${GREEN}	Type your Keyboard variant 2. Leave it plank for no changes.${NONE}\n"
			echo -e "${GREEN}	Example: 	${NONE}nodeadkeys\n"
			read -p "	 : " KEYVARB
			
			if ! [ -z $KEYVARB ]; then
				SET_KBVARIANTB="$KEYVARB"
				KEYLA+="-$KEYVARB"
			fi
			unset KEYVARB
		fi
		
		LINE_N=`sudo grep -n '/etc/rc.conf' -e 'KEYMAP=' | cut -d":" -f1`
		sudo sed -i -e ${LINE_N}c"KEYMAP=$KEYLA" /etc/rc.conf
		unset LINE_N
		set_xkb
		
		echo -e "\n${GREEN}	Keyboard layout is now ${NONE}$SET_KBLAYOUT\n"
		if ! [ -z $SET_KBVARIANTA ]; then
			echo -e "${GREEN}	Keyboard variant 1 is now ${NONE}$SET_KBVARIANTA\n"
			if ! [ -z $SET_KBVARIANTB ]; then
				echo -e "${GREEN}	Keyboard variant 2 is now ${NONE}$SET_KBVARIANTB\n"
			fi
		fi
		
		if ! [ -f $SCRIPT_PATH/keyboard-layout.txt ]; then
			sudo touch $SCRIPT_PATH/keyboard-layout.txt
		fi
		echo "$SET_KBLAYOUT" | sudo tee $SCRIPT_PATH/keyboard-layout.txt > /dev/null
		echo "$SET_KBVARIANTA" | sudo tee -a $SCRIPT_PATH/keyboard-layout.txt > /dev/null
		echo "$SET_KBVARIANTB" | sudo tee -a $SCRIPT_PATH/keyboard-layout.txt > /dev/null
		
		press_continue
		unset KEYVARA
	fi
	unset KEYLA
}


choose_timezone () {
	show_head " - Timezone"
	echo -e "\n${GREEN}	Type your Region. Leave it plank for no changes.${NONE}\n"
	echo -e "${GREEN}	Example: 	${NONE}Europe\n"
	echo -e "\n${GREEN}	Available Regions: ${NONE}\n"
	ls /usr/share/zoneinfo/
	echo -e "\n${GREEN}	Your Region?${NONE}\n"
	read -p "	 : " REG_CHOICE
	
	if ! [ -z $REG_CHOICE ]; then
		ZONE="$REG_CHOICE"
		if [ -f /usr/share/zoneinfo/$REG_CHOICE ]; then
			set_timezone $ZONE
			
		elif [ -d /usr/share/zoneinfo/$REG_CHOICE ]; then
			echo -e "\n${GREEN}	Type the Name of the capital City.${NONE}\n"
			echo -e "${GREEN}	Example: 	${NONE}Berlin\n"
			echo -e "\n${GREEN}	Available Citys: ${NONE}\n"
			ls /usr/share/zoneinfo/$REG_CHOICE
			echo -e "\n${GREEN}	${NONE}\n"
			read -p "	 : " city
			
			if [ -f /usr/share/zoneinfo/$REG_CHOICE/$city ]; then
				ZONE+="/$city" 
				set_timezone $ZONE
			else
				echo -e "\n${GREEN}	The City ${NONE}$city${GREEN} doesn't exist. No changes!${NONE}\n"
				press_continue
			fi
			unset city
		
		elif ! [ -f /usr/share/zoneinfo/$REG_CHOICE ] && ! [ -d /usr/share/zoneinfo/$REG_CHOICE ]; then
			echo -e "\n${GREEN}	This Region ${NONE}$REG_CHOICE${GREEN} doesn't exist. No changes!${NONE}\n"
			press_continue
		fi
		unset ZONE
	fi
	unset REG_CHOICE
}


set_timezone () {
	LINE_N=`sudo grep -n '/etc/rc.conf' -e 'TIMEZONE=' | cut -d":" -f1`
	sudo sed -i -e ${LINE_N}c"TIMEZONE=$1" /etc/rc.conf
	sudo ln -sf /usr/share/zoneinfo/$1 /etc/localtime
	echo -e "\n${GREEN}	Timezone is now ${NONE}$1\n"
	press_continue
}


change_locales () {
	while true; do
		show_head " - Change Locales"
		echo -e "\n${GREEN}	Setup Language, Keyboard Layout and Timezone .
	Be careful! You should know what you write!${NONE}\n"
		read -p "	1 ${GREEN}Set Language 	(UTF-8)${NONE}
	2 ${GREEN}Set Keyboard layout${NONE}
	3 ${GREEN}Set Timezone${NONE}
		
	0 ${GREEN}Back${NONE}

	 : " LOC_CHOICE
	 
		 if [ "$LOC_CHOICE" = "0" ]; then
		 	break
		 elif [ "$LOC_CHOICE" = "1" ]; then
		 	set_lang
		 elif [ "$LOC_CHOICE" = "2" ]; then
		 	set_key_layout
		 elif [ "$LOC_CHOICE" = "3" ]; then
		 	choose_timezone
	 	fi
	done
	unset LOC_CHOICE
}


install_desktop () {
	LXQT=" "
	XFCE=" "
	LUMINA=" "
	CINNAMON=" "
	KDE=" "
	GNOME=" "
	DESKTOP="      "
	while true; do
		show_head " - Install Desktop"
		echo -e "\n${GREEN}	You can install one or more Desktop(s).${NONE}\n"
		echo -e "\n${NONE}	1 ${GREEN}LXQT${NONE}					[ ${GREEN}$LXQT${NONE} ]
	2 ${GREEN}XFCE${NONE}					[ ${GREEN}$XFCE${NONE} ]
	3 ${GREEN}Lumina${NONE}				[ ${GREEN}$LUMINA${NONE} ]
	4 ${GREEN}Cinnamon${NONE}				[ ${GREEN}$CINNAMON${NONE} ]	

	5 ${GREEN}KDE${NONE}					[ ${GREEN}$KDE${NONE} ]
	6 ${GREEN}Gnome${NONE}					[ ${GREEN}$GNOME${NONE} ]

	9 ${GREEN}Install${NONE}				
	0 ${GREEN}Back${NONE}\n"
		read -p "	: " DE_CHOICE
		
		if [ "$DE_CHOICE" = "0" ]; then
			echo -e "\n${GREEN}	Installing no Desktop. Back to main Menu${NONE}\n"
			sleep 1
			DESKTOP="      "
			break
			
		elif [ "$DE_CHOICE" = "1" ]; then
			LXQT=$(toggle_var $LXQT " " "*")

		elif [ "$DE_CHOICE" = "2" ]; then
			XFCE=$(toggle_var $XFCE " " "*")
			
		elif [ "$DE_CHOICE" = "3" ]; then 
			LUMINA=$(toggle_var $LUMINA " " "*")

		elif [ "$DE_CHOICE" = "4" ]; then
			CINNAMON=$(toggle_var $CINNAMON " " "*")
					
		elif [ "$DE_CHOICE" = "5" ]; then
			KDE=$(toggle_var $KDE " " "*")

		elif [ "$DE_CHOICE" = "6" ]; then
			GNOME=$(toggle_var $GNOME " " "*")

		elif [ "$DE_CHOICE" = "9" ]; then
			DESKTOP="$LXQT$XFCE$LUMINA$CINNAMON$KDE$GNOME"
			break
		fi
	done
	
	unset DE_CHOICE

	if ! [ "$DESKTOP" = "      " ]; then
		USERS=$(ls /home | sed /lost+found/d)
		for USER in $USERS; do
			cp_directory $CONTENT_PATH/.config /home/$USER
			sudo chown $USER:$USER /home/$USER/.config
			sudo chmod 755 /home/$USER/.config
			
			sudo mkdir /home/$USER/.cache
			sudo chown $USER:$USER /home/$USER/.cache
			sudo chmod 755 /home/$USER/.cache
		done
		unset USERS
		
		if [ $(arch) = "x86_64" ]; then
			PACKAGES_TO_INSTALL_F+=" mesa-dri"
		elif [ $(arch) = "aarch64" ]; then
			PACKAGES_TO_INSTALL_F+=" mesa-panfrost-dri"
		fi

		PACKAGES_TO_INSTALL_F+=" xorg-minimal setxkbmap wayland dbus-elogind dbus-elogind-x11 elogind"
		SERVICES_TO_ENABLE_F+=( "dbus" "elogind" )
		
		PACKAGES_TO_INSTALL+=" alsa-utils alsa-ucm-conf libspa-alsa pipewire alsa-pipewire gstreamer1-pipewire wireplumber wireplumber-elogind"
		SERVICES_TO_ENABLE+=( "alsa" )
		
		if [ "$LXQT" = "*" ]; then 
			PACKAGES_TO_INSTALL+=" lxqt gvfs gvfs-mtp FeatherPad qlipper"
			DESKTOP_TO_INSTALL+="LXQT"
		fi
		
		if [ "$XFCE" = "*" ]; then 
			PACKAGES_TO_INSTALL+=" xfce4 xfce4-battery-plugin xfce4-clipman-plugin xfce4-cpufreq-plugin xfce4-cpugraph-plugin xfce4-dict xfce4-diskperf-plugin xfce4-fsguard-plugin xfce4-genmon-plugin xfce4-mailwatch-plugin xfce4-mpc-plugin xfce4-netload-plugin  xfce4-panel-appmenu xfce4-places-plugin xfce4-pulseaudio-plugin xfce4-screenshooter xfce4-sensors-plugin xfce4-systemload-plugin xfce4-timer-plugin xfce4-verve-plugin xfce4-whiskermenu-plugin xfce4-xkb-plugin Thunar thunar-volman thunar-archive-plugin thunar-media-tags-plugin ristretto xarchiver mousepad zathura zathura-pdf-poppler gvfs gvfs-mtp gvfs-gphoto2 xfce-polkit parole"
			DESKTOP_TO_INSTALL+="XFCE "
		fi
		
		if [ "$LUMINA" = "*" ]; then 
			PACKAGES_TO_INSTALL+=" lumina lumina-pdf lumina-calculator gvfs gvfs-mtp gvfs-gphoto2 mousepad viewnior qterminal"
			DESKTOP_TO_INSTALL+="Lumina "
		fi
		
		if [ "$CINNAMON" = "*" ]; then 
			PACKAGES_TO_INSTALL+=" cinnamon gnome-system-monitor gnome-terminal gnome-screenshot gnome-disk-utility gnome-keyring gedit gedit-plugins evince gvfs gvfs-mtp gvfs-gphoto2 eog eog-plugins file-roller adwaita-plus gufw gettext colord rtkit ufw"
			DESKTOP_TO_INSTALL+="Cinnamon "
		fi
		
		if [ "$KDE" = "*" ]; then 
			PACKAGES_TO_INSTALL+=" kde5 kde5-baseapps plasma-firewall ufw ark partitionmanager gwenview kcalc spectacle"
			DESKTOP_TO_INSTALL+="KDE "
		fi
		
		if [ "$GNOME" = "*" ]; then 
			PACKAGES_TO_INSTALL+=" gnome nautilus nautilus-sendto gvfs gvfs-mtp gvfs-gphoto2 eog eog-plugins evince gedit gedit-plugins gnome-video-effects gnome-themes-extra gnome-session gnome-screenshot gnome-shell-extensions gnome-icon-theme gnome-icon-theme-extras gnome-icon-theme-symbolic gnome-backgrounds file-roller chrome-gnome-shell qt5ct"
			DESKTOP_TO_INSTALL+="Gnome "
		fi
		
		PACKAGES_TO_INSTALL+=" autofs octoxbps qt5ct xz xdg-user-dirs xdg-user-dirs-gtk xdg-utils gsfonts cantarell-fonts dejavu-fonts-ttf freefont-ttf terminus-font"
		SERVICES_TO_ENABLE+=( "NetworkManager" "polkitd" "colord" "rtkit" "ufw" )
		choose_dm
		
		show_head " - Install Desktop"
		
		echo -e "\n${GREEN}	Odroid Modell:		${NONE}$ODROID_MODEL\n"
		echo -e "\n${GREEN}	Desktop(s):		${NONE}$DESKTOP_TO_INSTALL\n"
		echo -e "\n${GREEN}	Display Manager:	${NONE}$DISPLAY_MANAGER\n"
		echo -e "\n${GREEN}	Packages to Install:	${NONE}\n"
		echo -e "	$PACKAGES_TO_INSTALL_F$PACKAGES_TO_INSTALL\n"
		
		read -p "	Continue? (y/n): " c
	
		if [ "$c" = "y" ]; then
			sudo xbps-install -Sy $PACKAGES_TO_INSTALL_F
			for S in ${SERVICES_TO_ENABLE_F[@]}; do
				enable_service $S
			done
			sleep 1
			
			sudo xbps-install -Sy $PACKAGES_TO_INSTALL
			setup_dm
			
			for S in ${SERVICES_TO_ENABLE[@]}; do
				enable_service $S
			done
			sleep 1
			
			set_xkb
			setup_pipewire
			setup_amixer
			reconfig_clean
			DO_REBOOT=1
		fi
		PACKAGES_TO_INSTALL=""
		SERVICES_TO_ENABLE=()
	fi
	unset {LXQT,XFCE,LUMINA,CINNAMON,KDE,GNOME,DESKTOP,DE_CHOICE}
}


choose_dm () {
	show_head " - Choose Display Manager"
	echo -e "\n${GREEN}	Choose one Display Manager you want to install.${NONE}\n"
	echo -e "\n${GREEN}	If you use Gnome-Desktop then choose GDM. 
	Otherwise choose LightDM.${NONE}\n"
	echo -e "\n${NONE}		   	LXQE		XFCE		Lumina		Cinnamon	KDE		Gnome
	1 ${GREEN}LightDM${NONE}	   +		+		+		+		+		-
	2 ${GREEN}LXDM${NONE}		   +		+		-		+		x		?
	3 ${GREEN}GDM${NONE}		   ?		?		?		?		?		+
	
	0 ${GREEN}NONE${NONE}

	- = don't work yet,		+ = work,	/ = work partial,	x = work on X only.	? = not tested${NONE}\n"

	 read -p "	: " DM_CHOICE
	
	if [ "$DM_CHOICE" = "0" ]; then
		DM_CHOICE=""
		echo -e "\n${GREEN}	Installing no Display Manager. Back to main Menu${NONE}\n"
		press_continue
		
	else
		if [ -d /var/service/lxdm ]; then
			sudo sv down lxdm
			sudo rm -r /var/service/lxdm
		fi
		
		if [ -d /var/service/gdm ]; then
			sudo sv down gdm
			sudo rm -r /var/service/gdm
		fi
		
		if [ -d /var/service/lightdm ]; then
			sudo sv down lightdm
			sudo rm -r /var/service/lightdm
		fi
		
		if [ "$DM_CHOICE" = "1" ]; then
			PACKAGES_TO_INSTALL+=" lightdm lightdm-gtk3-greeter lightdm-gtk-greeter-settings breeze-gtk breeze-icons"
			DISPLAY_MANAGER="lightdm"
			
		elif [ "$DM_CHOICE" = "2" ]; then
			PACKAGES_TO_INSTALL+=" lxdm lxdm-theme-vdojo"
			DISPLAY_MANAGER="lxdm"
			
		elif [ "$DM_CHOICE" = "3" ]; then
			PACKAGES_TO_INSTALL+=" gdm"
			DISPLAY_MANAGER="gdm"
		fi
	fi
	
	unset DM_CHOICE
}


packages_menu () {
	FLATPAK=" "
	FILEZILLA=" "
	FIREFOX=" "
	VLC=" "
	GUFW=" "
	GIMP=" "
	LIBREOFFICE=" "
	TELEGRAM=" "
	PACKAGES="        "
	
	while true; do
		show_head " - Install Packages"
		echo -e "\n${NONE}	Choose choose Packages to install.${NONE}\n"
		echo -e "\n	1${GREEN} Flatpak${NONE}				[ ${GREEN}$FLATPAK${NONE} ]
	2 ${GREEN}Filezilla${NONE}				[ ${GREEN}$FILEZILLA${NONE} ]
	3 ${GREEN}Firefox${NONE}				[ ${GREEN}$FIREFOX${NONE} ]
	4 ${GREEN}VLC${NONE}					[ ${GREEN}$VLC${NONE} ]
	5 ${GREEN}GUFW${NONE}					[ ${GREEN}$GUFW${NONE} ]
	6 ${GREEN}Gimp${NONE}					[ ${GREEN}$GIMP${NONE} ]
	7 ${GREEN}LibreOffice 	(flatpak)${NONE}		[ ${GREEN}$LIBREOFFICE${NONE} ]
	8 ${GREEN}Telegram Desktop${NONE}			[ ${GREEN}$TELEGRAM${NONE} ]
	
	9 ${GREEN}Install${NONE}
	0 ${GREEN}Back${NONE}\n"

	read -p "	 : " PK_CHOICE
	
		if [ "$PK_CHOICE" = "0" ]; then
			PACKAGES="        "
			PACKAGES_TO_INSTALL=""
			break
			
		elif [ "$PK_CHOICE" = "1" ]; then
			FLATPAK=$(toggle_var $FLATPAK " " "*")

		elif [ "$PK_CHOICE" = "2" ]; then
			FILEZILLA=$(toggle_var $FILEZILLA " " "*")
			
		elif [ "$PK_CHOICE" = "3" ]; then 
			FIREFOX=$(toggle_var $FIREFOX " " "*")

		elif [ "$PK_CHOICE" = "4" ]; then
			VLC=$(toggle_var $VLC " " "*")
					
		elif [ "$PK_CHOICE" = "5" ]; then
			GUFW=$(toggle_var $GUFW " " "*")
			
		elif [ "$PK_CHOICE" = "6" ]; then
			GIMP=$(toggle_var $GIMP " " "*")
			
		elif [ "$PK_CHOICE" = "7" ]; then
			LIBREOFFICE=$(toggle_var $LIBREOFFICE " " "*")
		
		elif [ "$PK_CHOICE" = "8" ]; then
			TELEGRAM=$(toggle_var $TELEGRAM " " "*")

		elif [ "$PK_CHOICE" = "9" ]; then
			PACKAGES="$FLATPAK$FILEZILLA$FIREFOX$VLC$GUFW$GIMP$LIBREOFFICE$TELEGRAM"
			break
		fi
	done
	
	if ! [ "$PACKAGES" = "        " ]; then
		if [ "$FLATPAK" = "*" ]; then
			PACKAGES_TO_INSTALL+=" flatpak"
		fi
		
		if [ "$FILEZILLA" = "*" ]; then
			PACKAGES_TO_INSTALL+=" filezilla"
		fi
		
		if [ "$FIREFOX" = "*" ]; then
			PACKAGES_TO_INSTALL+=" firefox"
		fi
		
		if [ "$VLC" = "*" ]; then
			PACKAGES_TO_INSTALL+=" vlc"
		fi
		
		if [ "$GUFW" = "*" ]; then
			PACKAGES_TO_INSTALL+=" gufw"
		fi
		
		if [ "$GIMP" = "*" ]; then
			PACKAGES_TO_INSTALL+=" gimp"
		fi
		
		if [ "$LIBREOFFICE" = "*" ]; then
			if [ "$FLATPAK" = " " ]; then
				PACKAGES_TO_INSTALL+=" flatpak"
			fi
		fi
		
		if [ "$TELEGRAM" = "*" ]; then
			PACKAGES_TO_INSTALL+=" telegram-desktop"
		fi
		
		echo -e "\n${GREEN}	Installing ${NONE}$PACKAGES_TO_INSTALL\n"
		read -p "	Continue? (y/n): " YN_CHOICE
		
		if [ "$YN_CHOICE" = "y" ]; then
			sudo xbps-install -Sy $PACKAGES_TO_INSTALL
			
			if [ "$LIBREOFFICE" = "*" ]; then
				xbps-fetch https://dl.flathub.org/repo/appstream/org.libreoffice.LibreOffice.flatpakref
				sudo flatpak install -y org.libreoffice.LibreOffice.flatpakref
				rm org.libreoffice.LibreOffice.flatpakref
			elif [ "$LIBREOFFICE" = " " ] && [ "$FLATPAK" = "*" ]; then
				sudo flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
			fi
		fi
		unset YN_CHOICE
	fi
	
	unset {FLATPAK,FILEZILLA,FIREFOX,VLC,GUFW,GIMP,LIBREOFFICE,TELEGRAM,PACKAGES,PK_CHOICE}
	PACKAGES_TO_INSTALL=""
}


services_menu () {
	while true; do
		show_head " - Services"
		echo -e "\n${GREEN}	Enable or Disable Service${NONE}\n"
		read -p "	1 ${GREEN}Enable Service)${NONE}
	2 ${GREEN}Disable Service${NONE}
		
	0 ${GREEN}Back${NONE}

	 : " SERVICES_CHOICE
	 
		 if [ "$SERVICES_CHOICE" = "0" ]; then
		 	break
		 elif [ "$SERVICES_CHOICE" = "1" ]; then
		 	user_enable_service
		 elif [ "$SERVICES_CHOICE" = "2" ]; then
		 	user_disable_service
	 	fi
	done
	unset SERVICES_CHOICE
}


user_enable_service () {
	while true; do
		show_head " - Enable Service"
		echo -e "\n	${GREEN}Available Services:${NONE}\n"
		ls /etc/sv
		echo -e "\n	${GREEN}Active Services:${NONE}\n"
		ls /var/service/
		
		echo -e "\n${NONE}	Type the Name of the Service you want to enable. Leave it plank for no changes.${NONE}\n"

		read -p "	 : " SERVICE_NAME
		
		if [ -z $SERVICE_NAME ]; then
			break
			
		else
			if [ -d /etc/sv/$SERVICE_NAME ]; then
				ln /etc/sv/$SERVICE_NAME /var/service/
			else
				echo -e "\n${GREEN}	Service $SERVICE_NAME not found.${NONE}\n"
				sleep 2
			fi
		fi
		unset SERVICE_NAME
	done
}


user_disable_service () {	
	while true; do
		show_head " - Disable Service"
		echo -e "\n	${GREEN}Available Services:${NONE}\n"
		ls /etc/sv
		echo -e "\n	${GREEN}Active Services:${NONE}\n"
		ls /var/service/
		
		echo -e "\n${NONE}	Type the Name of the Service you want to disable. Leave it plank for no changes.${NONE}\n"

		read -p "	 : " SERVICE_NAME
		
		if [ -z $SERVICE_NAME ]; then
			break
		else
			if [ -d /etc/sv/$SERVICE_NAME ]; then
				rm -r /var/service/$SERVICE_NAME
			else
				echo -e "\n${GREEN}	Service $SERVICE_NAME not found.${NONE}\n"
				sleep 2
			fi
		fi
		unset SERVICE_NAME
	done
}


kernel_options () {
	if [ -f /etc/kernel.d/pre-install/90-vkpurge ]; then
		REMOVE_OLD_KERNEL="yes"
	else
		REMOVE_OLD_KERNEL="no"
	fi

	while true; do
		show_head " - Kernel options"
		echo -e "\n	1 ${GREEN}Remove old kernels in /boot during update${NONE}		[ ${GREEN}$REMOVE_OLD_KERNEL${NONE} ]\n"

		echo -e "\n	2 ${GREEN}Install linux-lts and its headers${NONE}"
		echo -e "	3 ${GREEN}Install linux and its headers${NONE}\n"

		echo -e "\n	4 ${GREEN}Remove linux-lts${NONE}"
		echo -e "	5 ${GREEN}Remove linux and${NONE}\n"

		echo -e "	0 ${GREEN}Back${NONE}\n"

		read -p "	 : " K_OPTION

		if [ "$K_OPTION" = "0" ]; then
			break

		elif [ "$K_OPTION" = "1" ]; then
			if [ "$REMOVE_OLD_KERNEL" = "yes" ]; then
				REMOVE_OLD_KERNEL="no"
				sudo rm /etc/kernel.d/pre-install/90-vkpurge
			else
				REMOVE_OLD_KERNEL="yes"
				sudo ln -s $SCRIPT_PATH/90-vkpurge /etc/kernel.d/pre-install/
			fi

		elif [ "$K_OPTION" = "2" ]; then
			touch /etc/xbps.d/linux-lts-headers.conf
			echo "virtualpkg=linux-headers:linux-lts-headers" | tee -a /etc/xbps.d/linux-lts-headers.conf
			sudo xbps-install -S linux-lts linux-lts-headers
			sudo xbps-pkgdb -m repolock linux-lts linux-lts-headers linux5.15 linux5.15-headers

		elif [ "$K_OPTION" = "3" ]; then
			sudo rm /etc/xbps.d/linux-lts-headers.conf
			sudo xbps-install -S linux linux-headers
			sudo xbps-pkgdb -m repolock linux linux-headers linux6.3 linux6.3-headers

		elif [ "$K_OPTION" = "4" ]; then
			sudo xbps-remove linux-lts linux5.15
			sudo xbps-remove linux-lts-headers linux5.15-headers

		elif [ "$K_OPTION" = "5" ]; then
			sudo xbps-remove linux linux6.3
			sudo xbps-remove linux-headers linux6.3-headers

		fi
		unset K_OPTION
	done
	unset REMOVE_OLD_KERNEL
}


main_menu () {
	while true; do
		show_head " - Main Menu"
		echo -e "\n${NONE}	1 ${GREEN}Change password${NONE}
	2 ${GREEN}Create new User${NONE}
	3 ${GREEN}Change Locales${NONE}
	4 ${GREEN}Install Desktop${NONE}
	5 ${GREEN}Install Display Manager${NONE}
	6 ${GREEN}Install some Packages${NONE}

	7 ${GREEN}Update system${NONE}
	8 ${GREEN}Reconfig and Cleanup${NONE}
	9 ${GREEN}Services${NONE}
	0 ${GREEN}Kernel options${NONE}

	i ${GREEN}Install Void Odroid	(new rootf)${NONE}
	u ${GREEN}Update setup-void${NONE}
	
	r ${GREEN}Reboot${NONE}
	x ${GREEN}Exit${NONE}\n"
		read -p "	 : " MM_CHOICE
	 	
		if [ "$MM_CHOICE" = "x" ]; then
			exit_script
			break

		elif [ "$MM_CHOICE" = "r" ]; then
			DO_REBOOT=1
			exit_script
			break

		elif [ "$MM_CHOICE" = "i" ]; then
			sudo /bin/bash $SCRIPT_PATH/void-odroid-installer.sh

		elif [ "$MM_CHOICE" = "u" ]; then
			update_setup_void

		elif [ "$MM_CHOICE" = "1" ]; then
			change_passwd
		
		elif [ "$MM_CHOICE" = "2" ]; then
			create_new_user
			
		elif [ "$MM_CHOICE" = "3" ]; then
			change_locales
			
		elif [ "$MM_CHOICE" = "4" ]; then
			install_desktop
			
		elif [ "$MM_CHOICE" = "5" ]; then
			choose_dm
			if ! [ -z $DISPLAY_MANAGER ]; then
				sudo xbps-install -Sy $PACKAGES_TO_INSTALL
				setup_dm
				PACKAGES_TO_INSTALL=""
				echo -e "\n${GREEN}	Display Manager will be activated after exit setup-void.${NONE}\n"
			fi
			
		elif [ "$MM_CHOICE" = "6" ]; then
			packages_menu

		elif [ "$MM_CHOICE" = "7" ]; then
			update_system

		elif [ "$MM_CHOICE" = "8" ]; then
			reconfig_clean
		
		elif [ "$MM_CHOICE" = "9" ]; then
			services_menu

		elif [ "$MM_CHOICE" = "0" ]; then
			kernel_options

		fi
	done
	unset MM_CHOICE
}


if grep $SCRIPT_NAME ~/.bash_profile; then
	change_passwd_root
	sudo /bin/bash $SCRIPT_PATH/install-base.sh
	delete_autostart
fi

if [ -f $SCRIPT_PATH/keyboard-layout.txt ]; then
	SET_KBLAYOUT="`sed -n '1p' < $SCRIPT_PATH/keyboard-layout.txt`"
	SET_KBVARIANTA="`sed -n '2p' < $SCRIPT_PATH/keyboard-layout.txt`"
	SET_KBVARIANTB="`sed -n '3p' < $SCRIPT_PATH/keyboard-layout.txt`"
fi

main_menu

exit
