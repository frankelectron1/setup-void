# /bin/bash

SCRIPT_PATH=/opt/setup-void

sleep 6

# update Void Linux
xbps-install -Suy xbps
xbps-install -Suy

# install linux-base and some packages
xbps-install -Sy git htop libsensors linux-base wget xz gzip

# reconfigure and clean up
xbps-reconfigure -fa
xbps-remove -oO

# update setup-void
git -C $SCRIPT_PATH pull https://gitlab.com/frankelectron1/setup-void.git

exit 0
