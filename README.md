# setup-void

setup-void is a Script which can setup Void Linux and install Desktops. It is written for use on Odroid N2/N2+/C4/HC4 but can be used on other Machines.

## Features

- Install linux-base htop and libsensors at first start
- Update System at start
- Change Root Password
- Create new Users
- Change Locales (Language, Keyboard layout, Timezone)
- Install Desktop
- Install Display Manger
- Install Audio / Setup Audio for Odroid
- Install some Packages
- Reconfigure all Packages
- Enable/Disable Services
- Install/remove linux packages
- Update itself
- Execute void-odroid-installer.sh

# void-odroid-installer and setup-void-rootfs

void-odroid-installer.sh is a Script to Download Void Linux rootfs and set it up to run on Odroid N2/N2+/C4/HC4 via setup-void-rootfs.
On the new rootfs first login as user void will start the setup-void Script.

## Usage

Execute ./void-odroid-installer.sh in a Terminal.

## Features

- Create Partitions
- Make filesystem (ext4, btrfs, xfs)
- Mount Partitions
- Download and extract Void LInux rootfs
- Download and extract ODROIDBIOS for C4
- Clone these Repo into new rootfs
- Set Repo https://gitlab.com/frankelectron1/void-repo-frankelectron/-/raw/main/aarch64
- Install Linux Kernel from https://gitlab.com/frankelectron1/void-repo-frankelectron/-/raw/main/aarch64
- Install chrony
- Create /etc/kboot.conf to boot via Petiteboot
- Create user void
- Enable Services sshd dhcpcd chronyd
- Link some kernel hook files
- Pack rootfs into .tar.xz archive


## Dependencies

- wget
- git 
- tar
- xz
- gzip

## On other architectures

To run the installer Script on other architectures you need to install qemu-user-static and binfmt-support and enable the binfmt-support Service.

### Dependencies

- qemu-user-static
- binfmt-support
