#!/bin/bash
#set -e

GREEN=$(tput setaf 2)
NONE=$(tput sgr0)

SCRIPT_PATH=$(dirname "$(realpath "$0")")

ROOTFS_VERSION="$(sed -n '1p' < "$SCRIPT_PATH"/ROOTFS_VERSION.txt)"

FUNC_RETURN=""

ODROID_MODEL="	"
STORAGE_TYPE="	"
FILESYSTEM="	"
INSTALL_DIR="	"
DEV_OR_DIR="	"

INSTALL_DEV="	"
DEV_PART_ROOT="	"
DEV_PART_BOOT="	"

TARGET_DEV="	"
TARGET_DEV_MESS=""
PARTITION_ROOT="	"
PARTITION_BOOT="	"

BOOTLOADER_DEV="	"
BOOTLOADER_PART="	"
BOOTLOADER_MNT="	"

SHA_SUM=""
ARCH=$(arch)

show_header () {
	clear
	echo -e "\n${GREEN}Install Void on Odroid${NONE} $1\n"
}


press_continue () {
	echo -e "\n	${GREEN}Press enter to continue... ${NONE}\n"
	read -n 1 -r -s -p $"	"
}


wellcome () {
	show_header "Hello"
	echo -e "\n${GREEN}	This will create a root filesystem for Odroid N2/N2Plus/C4/HC4.${NONE}\n"

	echo -e "${GREEN}	There are options to install Void Linux onto a storage device or into a directory.${NONE}"
	echo -e "${GREEN}	Also you can pack the rootfs into a *tar.xz file. ${NONE}\n"

	echo -e "${GREEN}	Available storage devices and partitions: ${NONE}\n"
	echo -e "$(lsblk | sed 's/^/		/')\n"

	if ! [ "$ARCH" = "aarch64" ]; then
		echo -e "\n\n${GREEN}	The current system is a ${NONE}${ARCH}${GREEN} architecture!${NONE}"
		echo -e "${GREEN}	void-odroid-installer will use ${NONE}qemu-user-static ${GREEN}and ${NONE}binfmt-support${GREEN} to enter the aarch64 rootfs.${NONE}\n"
	fi

	press_continue
}


check_dependencies () {
	local _MISS_DEP=()
	local _MISS_SER=""

	for _dep in git tar wget xz gzip; do
		if ! [ -x /usr/bin/$_dep ]; then
			_MISS_DEP+=("$_dep")
		fi
	done

	if ! [ "$ARCH" = "aarch64" ]; then
		if ! [ -x /usr/bin/qemu-"$ARCH"-static ]; then
			_MISS_DEP+=("qemu-$ARCH-static")
		fi

		if ! [ "$(ps -eaf | grep -i "binfmt-support" | sed '/^$/d' | wc -l)" -ge 2 ]; then
			_MISS_SER="binfmt-support"
		fi
	fi

	if [ "${#_MISS_DEP[@]}" -ne 0 ] || [ -n "$_MISS_SER" ]; then
		show_header "Dependencies"

		if [ "${#_MISS_DEP[@]}" -ne 0 ]; then
			echo -e "\n	${GREEN} Packages not found! Please install ${NONE}\n"

			for _inst in "${_MISS_DEP[@]}"; do
				echo -e "$_inst"
			done
		fi

		if [ -n "$_MISS_SER" ]; then
			echo -e "\n	${GREEN} Services not found! Please enable ${NONE}\n"
			echo -e "$_MISS_SER"
		fi
		press_continue
		exit
	fi
}


give_dev () {
	local _GIVEN_DEV
	FUNC_RETURN=""

	echo -e "${GREEN}	! Be carefull and double check the device name !${NONE}\n"
	echo -e "$(lsblk | sed 's/^/		/')\n"
	echo -e "${GREEN}	Available storage devices: ${NONE}\n"
	echo -e "$(lsblk | grep -e NAME -e disk | sed 's/^/		/')\n"
	echo -e "${GREEN}	Enter the name of the device.${NONE}\n"

	read -p "	 : " _GIVEN_DEV

	if [ -n "$_GIVEN_DEV" ]; then
		if [ -e /dev/"$_GIVEN_DEV" ]; then
			FUNC_RETURN="$_GIVEN_DEV"
			return 0
		else
			echo -e "\n${GREEN}	The device $_GIVEN_DEV does not exist.${NONE}"
			press_continue
			return 1
		fi
	fi
}


give_partition () {
	# $1 = name of storage device (examble: sda)
	local _GIVEN_PART_N
	FUNC_RETURN=""

	echo -e "$(lsblk /dev/"$1" | grep -e NAME -e part | sed 's/^/		/')\n"
	echo -e "${GREEN}	Enter the name of the partition.${NONE}\n"

	read -p "	 : $1" _GIVEN_PART_N

	if [ -n "$_GIVEN_PART_N" ]; then
		if [ -e /dev/"$1$_GIVEN_PART_N" ]; then
			FUNC_RETURN="$_GIVEN_PART_N"
			return 0
		else
			echo -e "\n${GREEN}	The device $_GIVEN_PART_N does not exist.${NONE}"
			press_continue
			return 1
		fi
	fi
}


exit_script () {
	local _YN_CHOICE

	show_header "Exit"
	echo -e "\n$(lsblk | sed 's/^/		/')\n"
	echo -e "\n${GREEN}	CAUTION! \n"
	echo -e "\n${GREEN}	It is very important to sync and unmount devices before removing them! \n"
	echo -e "${GREEN}	In main menu is an option (u) to do this. ${NONE}\n"
	echo -e "\n${GREEN}	Back to main menu? ${NONE}\n"

	read -p "	(y/n): " _YN_CHOICE

	if [ "$_YN_CHOICE" = "n" ]; then
		exit
	fi
}


odroid_model () {
	local _MODEL

	show_header "Odroid Model"
	echo -e "\n${GREEN}	Select your Odroid model.${NONE}\n"
	echo -e "\n${NONE}	1 ${GREEN}N2${NONE}
	2 ${GREEN}N2Plus${NONE}
	3 ${GREEN}C4${NONE}
	4 ${GREEN}HC4${NONE}
	5 ${GREEN}M1${NONE}\n"
	read -p "	  : " _MODEL

	if [ "$_MODEL" = "1" ]; then
		ODROID_MODEL="N2"
	elif [ "$_MODEL" = "2" ]; then
		ODROID_MODEL="N2Plus"
	elif [ "$_MODEL" = "3" ]; then
		ODROID_MODEL="C4"
		echo -e "\n${GREEN}	To boot Void Linux on Odroid C4 installing Petiteboot onto SD-Card or EMMC with DOS partition table is required.${NONE}\n"
		echo -e "${GREEN}	The partition for Petiteboot musst be the first on the device and need a ext4 or fat filesysem. (It will be ext4.)${NONE}\n"
		echo -e "${GREEN}	Uboot will be installed in the MBR of the storage device.${NONE}\n"
		echo -e "${GREEN}	If you plan to boot from a USB device a SD-Card or EMMC with one ext4 partition for Petiteboot is still required.${NONE}\n"
		press_continue
	elif [ "$_MODEL" = "4" ]; then
		ODROID_MODEL="HC4"
	elif [ "$_MODEL" = "5" ]; then
		ODROID_MODEL="M1"
	fi
}


storage_type () {
	local _DEVICE_TYPE

	show_header "Storage Device Type"
	echo -e "\n${GREEN} Select the type of storage device Void Linux will be installed to.${NONE}\n"
	echo -e "\n${NONE}	1 ${GREEN}SD-Card${NONE}
	2 ${GREEN}EMMC${NONE}
	3 ${GREEN}USB/SSD (/dev/sda)${NONE}"
	if [ "$ODROID_MODEL" = "M1" ]; then
		echo -e "	4 ${GREEN}NVME (/dev/nvme0n1)${NONE}"
	fi
	echo -e "	"

	read -p "	 : " _DEVICE_TYPE

	if [ "$_DEVICE_TYPE" = "1" ]; then
		STORAGE_TYPE="SD-Card"
		TARGET_DEV="mmcblk1"
	elif [ "$_DEVICE_TYPE" = "2" ]; then
		STORAGE_TYPE="EMMC"
		TARGET_DEV="mmcblk0"
	elif [ "$_DEVICE_TYPE" = "3" ]; then
		STORAGE_TYPE="USB/SSD"
		TARGET_DEV="sda"
	elif [ "$_DEVICE_TYPE" = "4" ]; then
		STORAGE_TYPE="NVME"
		TARGET_DEV="nvme0n1"
	fi
}


filesystem_rootfs () {
	local _NEW_FS

	show_header "Filesystem"
	echo -e "\n${GREEN}	Select a filesystem for / (root).${NONE}\n"
	echo -e "\n	1 ${GREEN}ext4${NONE}
	2 ${GREEN}btrfs${NONE}
	3 ${GREEN}xfs${NONE}\n"

	read -p "	: " _NEW_FS

	if [ "$_NEW_FS" = "1" ]; then
		FILESYSTEM="ext4"
	elif [ "$_NEW_FS" = "2" ]; then
		FILESYSTEM="btrfs"
	elif [ "$_NEW_FS" = "3" ]; then
		FILESYSTEM="xfs"
	fi
}


install_dev () {
	show_header "Storage Device for / (root)"
	echo -e "\n${GREEN}	Storage device Void Linux will be installed to.${NONE}\n"

	give_dev
	if [ -n "$FUNC_RETURN" ]; then
		INSTALL_DEV="$FUNC_RETURN"
	fi
}


root_partition () {
	show_header "Partition for / (root)"
	if [ "$INSTALL_DEV" = "	" ]; then
		echo -e "\n${GREEN}	No device for rootfs! Please give a device first. ${NONE}\n"
		press_continue
	else
		echo -e "\n${GREEN}	Partition for / (root).${NONE}\n"
		give_partition "$INSTALL_DEV"
		if [ -n "$FUNC_RETURN" ]; then
			DEV_PART_ROOT="$FUNC_RETURN"
		fi
	fi
}


boot_partition () {
	show_header "Partition for /boot"
	if [ "$INSTALL_DEV" = "	" ]; then
		echo -e "\n${GREEN}	No device for rootfs! Please give a device first. ${NONE}\n"
		press_continue
	else
		echo -e "\n${GREEN}	Partition for /boot.${NONE}\n"
		give_partition "$INSTALL_DEV"
		if [ -n "$FUNC_RETURN" ]; then
			DEV_PART_BOOT="$FUNC_RETURN"
		fi
	fi
}


bootloader_dev () {
	show_header "Bootloader Device"
	echo -e "\n${GREEN}	Storage device Petiteboot for C4 will be installed to.${NONE}\n"

	give_dev
	if [ -n "$FUNC_RETURN" ]; then
		BOOTLOADER_DEV="$FUNC_RETURN"
	fi
}


bootloader_part () {
	show_header "Partition for Petiteboot"
	echo -e "\n${GREEN}	Partition Petiteboot for C4 will be installed to.${NONE}\n"
	echo -e "\n${GREEN}	C4 will boot with Petiteboot on first partition only.${NONE}\n"

	if [ "$BOOTLOADER_DEV" = "	" ]; then
		echo -e "\n${GREEN}	No device for Petiteboot! Please give a device first. ${NONE}\n"
		press_continue
	else
		echo -e "\n${GREEN}	Partition for /boot.${NONE}\n"
		give_partition "$BOOTLOADER_DEV"
		if [ -n "$FUNC_RETURN" ]; then
			BOOTLOADER_PART="$FUNC_RETURN"
		fi
	fi
}


install_dir () {
	local _ROOT_DIR

	show_header "Directory for / (root)"
	if [ "$DEV_OR_DIR" = "Device" ]; then
		echo -e "\n${GREEN}	Enter the absolute path and name of the directory which will be the mountpoint for partition for / (root).${NONE}\n"
	else
		echo -e "\n${GREEN}	Enter the absolute path and name of the directory for / (root).${NONE}\n"
	fi
	echo -e "\n${GREEN}	Example : ${NONE}/mnt/void-rootfs\n"

	read -p "	 : " _ROOT_DIR

	if [ -n "$_ROOT_DIR" ]; then
		if ! [ -e "$_ROOT_DIR" ]; then
			echo -e "\n${GREEN}	The directory will be created.${NONE}\n"
		else
			echo -e "\n${GREEN}	The directory must be empty.${NONE}"
		fi
		INSTALL_DIR=$_ROOT_DIR
		press_continue
	fi
}


target_part () {
	local _ROOT_PART_N

	show_header "Partition for / (root) on target Device"
	echo -e "\n${GREEN}	Enter the number of the partition which will contain / on target device.${NONE}\n"

	read -p "	 : " _ROOT_PART_N

	if [ -n "$_ROOT_PART_N" ]; then
		PARTITION_ROOT=$_ROOT_PART_N
	fi
}


new_partition () {
	show_header "New Partition"
	echo -e "\n${GREEN}	For ext4 filesysem at least one ext4 partition with at least 2GB must be created.${NONE}\n"
	echo -e "${GREEN}	For btrfs or xfs filesysem two partitions must be created.
	One ext4 partition with at least 300MB. This partition will have mountpoint as /boot.
	And at least one btrfs or xfs partition with at least 2GB is required for the rootfs (/).${NONE}\n"
	echo -e "\n${GREEN}	If install Void for Odroid C4 use DOS partition table otherwise you can use GPT partition table.${NONE}\n"

	press_continue

	show_header "New Partition"

	give_dev
	if [ -n "$FUNC_RETURN" ]; then
		cfdisk /dev/"$FUNC_RETURN"
	fi
}


wipe_storage () {
	show_header "Wipe Storage Device"
	echo -e "\n${GREEN}	This will wipe the storage device completely. All data and partitions on it will be lost.${NONE}\n"
	echo -e "${GREEN}	This will overwrite the device with 0 and will take a long time.${NONE}\n"

	give_dev
	if [ -n "$FUNC_RETURN" ]; then
		local _WIPE_STORAGE

		read -p "	Continue? (yes/no) : " _WIPE_STORAGE

		if [ "$_WIPE_STORAGE" = "yes" ]; then
			show_header "Wipe Storage Device"
			umount_dev "$FUNC_RETURN"
			dd if=/dev/zero of=/dev/"$FUNC_RETURN" bs=1M status=progress
			echo -e "\n${GREEN}	Wipe storage device successful.${NONE}\n"
			press_continue
		fi
	fi
}


pack_rootfs () {
	local _PM_CHOICE _ROOTFS_PATH=$INSTALL_DIR _TAR_PATH="	" _TAR_NAME="	" _THREAD_N="0" _COMP_LEVEL="6"

	if ! [ "$INSTALL_DIR" = "	" ]; then
		_TAR_PATH=$(dirname "$INSTALL_DIR")
		_TAR_NAME=$(echo "$INSTALL_DIR" | grep -o '[^/]*$')
	fi

	while true; do
		show_header "Pack rootfs into .tar.xz"
		echo -e "\n${GREEN}	This will pack the rootfs into a .tar.xz file.${NONE}"
		echo -e "${GREEN}	So you can share or unpack it onto a storage device later.${NONE}\n"

		echo -e "\n	1 ${GREEN}Directory of rootfs${NONE}					[ $_ROOTFS_PATH ]\n"
		echo -e "	2 ${GREEN}Path for .tar file${NONE}					[ $_TAR_PATH ]\n"
		echo -e "	3 ${GREEN}Name for .tar file${NONE}					[ $_TAR_NAME ]	.tar.xz\n"
		echo -e "	4 ${GREEN}Number of threads used to compress${NONE}			[ $_THREAD_N ]\n"
		echo -e "	5 ${GREEN}Compression level${NONE}					[ $_COMP_LEVEL ]\n"

		echo -e "\n	9 ${GREEN}Pack${NONE}\n"
		echo -e "	0 ${GREEN}Back${NONE}\n"

		read -p "	 : " _PM_CHOICE

		if [ "$_PM_CHOICE" = "0" ]; then
			break

		elif [ "$_PM_CHOICE" = "1" ]; then
			read -p "	Enter the absolute path of rootfs : " _ROOTFS_PATH

			if ! [ -e "$_ROOTFS_PATH" ]; then
				echo -e "\n${GREEN}	$_ROOTFS_PATH doesn't exist!${NONE}\n"
				_ROOTFS_PATH="	"
				press_continue
			fi

		elif [ "$_PM_CHOICE" = "2" ]; then
			read -p "	Enter the absolute path for .tar file : " _TAR_PATH

		elif [ "$_PM_CHOICE" = "3" ]; then
			read -p "	Enter the name for .tar file : " _TAR_NAME

		elif [ "$_PM_CHOICE" = "4" ]; then
			read -p "	Enter the number of threads to use (0 = all available) : " _THREAD_N

		elif [ "$_PM_CHOICE" = "5" ]; then
			read -p "	Enter compression level (0 .. 9) : " _COMP_LEVEL

		elif [ "$_PM_CHOICE" = "9" ]; then
			if ! [ "$_ROOTFS_PATH" = "	" ] && [ -n "$_ROOTFS_PATH" ] && ! [ "$_TAR_PATH" = "	" ] && [ -n "$_TAR_PATH" ] && ! [ "$_TAR_NAME" = "	" ] && [ -n "$_TAR_NAME" ]; then
				local  _USER _GROUP

				if ! [ -e "$_TAR_PATH" ]; then
					mkdir -p "$_TAR_PATH"
				fi
				(cd "$_ROOTFS_PATH" || return; XZ_OPT="-T${_THREAD_N} -${_COMP_LEVEL}" tar -cvpJf "$_TAR_PATH"/"$_TAR_NAME".tar.xz .)
				 _USER=$(whoami)
				_GROUP=$(groups | cut -d" " -f1)
				chown "$_USER:$_GROUP" "$_TAR_PATH"/"$_TAR_NAME".tar.xz

				echo -e "\n${GREEN}	Packing rootfs successful.${NONE}\n"
				press_continue
			else
				echo -e "\n${GREEN}	Something is missing. Check your input.${NONE}\n"
				press_continue
			fi
		fi

	done
}


umount_dev () {
	if [ -e /dev/"$1" ]; then
		local _MOUNTPOINTS

		_MOUNTPOINTS="$(lsblk -no mountpoints /dev/"$1" | grep "/")"
		if [ -n "${_MOUNTPOINTS}" ]; then
			for m in "${_MOUNTPOINTS[@]}"; do
				sync
				umount -R "$m"
			done
		else
			echo -e "\n${GREEN}	No mounted partition on this device! ${NONE}\n"
			press_continue
		fi
	fi
}


unmount_device () {
	show_header "Unmount Storage Device"
	echo -e "\n${GREEN}	This will unmount all partitions of a given storage device${NONE}\n"

	give_dev
	if [ -n "$FUNC_RETURN" ]; then
		umount_dev "$FUNC_RETURN"
		echo -e "\n$(lsblk | sed 's/^/		/')\n"
		press_continue
	fi
}


install_odroidbios () {
	local _YN_CHOICE

	echo -e "\n${GREEN}	Install Petiteboot for C4? ${NONE}\n"

	if [ "$DEV_OR_DIR" = "Device" ]; then
		echo -e "\n${GREEN}	Target device:	 ${NONE} $BOOTLOADER_DEV \n"
	fi

	read -p "	(y/n): " _YN_CHOICE

	if [ "$_YN_CHOICE" = "y" ]; then
		if ! [ -f "$SCRIPT_PATH"/ODROIDBIOS-20220315.tar.gz ]; then
			wget http://ppa.linuxfactory.or.kr/images/petitboot/odroidc4/ODROIDBIOS-20220315.tar.gz -P "$SCRIPT_PATH"
		fi

		tar -xvzf "$SCRIPT_PATH"/ODROIDBIOS-20220315.tar.gz -C "$INSTALL_DIR"/boot/ODROIDBIOS
		#cp -a $INSTALL_DIR/boot/ODROIDBIOS-20220315/ODROIDBIOS.BIN $BOOTLOADER_MNT
		wget http://ppa.linuxfactory.or.kr/images/petitboot/odroidc4/ODROIDBIOS.BIN -P "$BOOTLOADER_MNT"

		if  [ "$STORAGE_TYPE" = "USB/SSD" ]; then
			cp -a "$INSTALL_DIR"/boot/ODROIDBIOS/ODROIDBIOS-20220315/mmcboot.img "$BOOTLOADER_MNT"
		fi

		if [ "$DEV_OR_DIR" = "Device" ]; then
			(cd "$INSTALL_DIR"/boot/ODROIDBIOS/ODROIDBIOS-20220315 || return; /bin/bash "$INSTALL_DIR"/boot/ODROIDBIOS/ODROIDBIOS-20220315/sd_fusing.sh /dev/"$BOOTLOADER_DEV")
		fi

		rm "$SCRIPT_PATH"/ODROIDBIOS-20220315.tar.gz
	fi
}


check_install_parameters () {
	local _PARAM1="" _PARAM2="" _PARAM3="" _PARAM4=""

	if ! [ "$ODROID_MODEL" = "	" ] && ! [ "$STORAGE_TYPE" = "	" ] && ! [ "$DEV_OR_DIR" = "	" ] && ! [ "$INSTALL_DIR" = "	" ]; then
		_PARAM1="ok"
		if  [ "$DEV_OR_DIR" = "Device" ]; then
			if ! [ "$FILESYSTEM" = "	" ] && ! [ "$INSTALL_DEV" = "	" ] && ! [ "$DEV_PART_ROOT" = "	" ]; then
				_PARAM2="ok"
				if ! [ "$FILESYSTEM" = "ext4" ]; then
					if ! [ "$DEV_PART_BOOT" = "	" ];then
						_PARAM3="ok"
					fi
				else
					_PARAM3="ok"
				fi
				if [ "$ODROID_MODEL" = "C4" ] && [ "$STORAGE_TYPE" = "USB/SSD" ]; then
					if ! [ "$BOOTLOADER_DEV" = "	" ] && ! [ "$BOOTLOADER_PART" = "	" ]; then # && !  [ "$BOOTLOADER_MNT" = "	" ]; then
						_PARAM4="ok"
					fi
				else
					_PARAM4="ok"
				fi
			fi
		else
			if ! [ "$PARTITION_ROOT" = "	" ];then
				_PARAM2="ok"
			fi
			_PARAM3="ok"
			_PARAM4="ok"
		fi
	fi

	if [ "$_PARAM1" = "ok" ] && [ "$_PARAM2" = "ok" ] && [ "$_PARAM3" = "ok" ] && [ "$_PARAM4" = "ok" ]; then
		echo "ok"
	else
		echo ""
	fi
}


prepare_storage () {
	mkfs."$FILESYSTEM" /dev/"$INSTALL_DEV""$DEV_PART_ROOT"
	mount /dev/"$INSTALL_DEV""$DEV_PART_ROOT" "$INSTALL_DIR"

	if ! [ "$FILESYSTEM" = "ext4" ]; then
		mkfs.ext4 /dev/"$INSTALL_DEV""$DEV_PART_BOOT"
		if ! [ -d "$INSTALL_DIR"/boot ]; then
			mkdir -p "$INSTALL_DIR"/boot
		fi
		mount /dev/"$INSTALL_DEV""$DEV_PART_BOOT" "$INSTALL_DIR"/boot
	fi

	if [ "$ODROID_MODEL" = "C4" ]; then
		if ! [ -d "$INSTALL_DIR"/boot/ODROIDBIOS ]; then
			mkdir -p "$INSTALL_DIR"/boot/ODROIDBIOS
		fi
		if [ "$STORAGE_TYPE" = "USB/SSD" ]; then
			mkfs.ext4 /dev/"$BOOTLOADER_DEV""$BOOTLOADER_PART"
			if ! [ -d "$BOOTLOADER_MNT" ]; then
				mkdir -p "$BOOTLOADER_MNT"
			fi
			mount /dev/"$BOOTLOADER_DEV""$BOOTLOADER_PART" "$BOOTLOADER_MNT"
		fi
	fi
}


install_rootfs () {
	if ! [ -f "$SCRIPT_PATH"/void-aarch64-ROOTFS-"$ROOTFS_VERSION".tar.xz ]; then
		wget https://repo-default.voidlinux.org/live/"$ROOTFS_VERSION"/void-aarch64-ROOTFS-"$ROOTFS_VERSION".tar.xz -P "$SCRIPT_PATH"
	fi

	if ! [ -f "$SCRIPT_PATH"/sha256sum.txt ]; then
		wget https://repo-default.voidlinux.org/live/"$ROOTFS_VERSION"/sha256sum.txt -P "$SCRIPT_PATH"
	fi

	SHA_SUM=$(cd "$SCRIPT_PATH" || return; sha256sum -c --ignore-missing sha256sum.txt | cut -d' ' -f2)
	echo "$SHA_SUM"

	if ! [ "$SHA_SUM" = "OK" ]; then
		rm "$SCRIPT_PATH"/void-aarch64-ROOTFS-"$ROOTFS_VERSION".tar.xz
		rm "$SCRIPT_PATH"/sha256sum.txt

		echo -e "\n${GREEN} Verification of ${NONE}void-aarch64-ROOTFS-$ROOTFS_VERSION.tar.xz ${GREEN}faild!${NONE}\n"
		echo -e "\n${GREEN} Try it again.${NONE}\n"
		press_continue

	else
		echo -e "\n${GREEN} Verification of ${NONE}void-aarch64-ROOTFS-$ROOTFS_VERSION.tar.xz ${GREEN}successful!${NONE}\n"
		sleep 3
		tar xvf "$SCRIPT_PATH"/void-aarch64-ROOTFS-"$ROOTFS_VERSION".tar.xz -C "$INSTALL_DIR"
		rm "$SCRIPT_PATH"/void-aarch64-ROOTFS-"$ROOTFS_VERSION".tar.xz
		rm "$SCRIPT_PATH"/sha256sum.txt

		git clone https://gitlab.com/frankelectron1/setup-void.git "$INSTALL_DIR"/opt/setup-void/

		if [ "$ODROID_MODEL" = "C4" ]; then
			install_odroidbios
		fi

		echo "/dev/$TARGET_DEV_MESS$PARTITION_ROOT / $FILESYSTEM defaults 0 1" | tee -a "$INSTALL_DIR"/etc/fstab

		if [ "$DEV_OR_DIR" = "Device" ]; then
			if ! [ "$FILESYSTEM" = "ext4" ]; then
				echo "/dev/$TARGET_DEV_MESS$PARTITION_BOOT /boot ext4 defaults 0 0" | tee -a "$INSTALL_DIR"/etc/fstab
			fi

			echo -e "\n${GREEN}	Syncing data ${NONE}\n"
			sync
		fi
	fi
}


parameters_to_file () {
	echo "ODROID_MODEL=$ODROID_MODEL" | tee "$INSTALL_DIR"/opt/setup-void/install_parameter.txt
	echo "FILESYSTEM=$FILESYSTEM" | tee -a "$INSTALL_DIR"/opt/setup-void/install_parameter.txt
	echo "TARGET_DEV=$TARGET_DEV" | tee -a "$INSTALL_DIR"/opt/setup-void/install_parameter.txt
	echo "PARTITION_ROOT=$PARTITION_ROOT" | tee -a "$INSTALL_DIR"/opt/setup-void/install_parameter.txt
	echo "PARTITION_BOOT=$PARTITION_BOOT" | tee -a "$INSTALL_DIR"/opt/setup-void/install_parameter.txt
}


chroot_rootfs () {
	local _ENTER_ROOTF

	if ! [ "$ARCH" = "aarch64" ]; then
		cp /usr/bin/qemu-aarch64-static "$INSTALL_DIR"/usr/bin
	fi

	trap unmount_rootfs INT

	for _fs in dev proc sys; do
		mount --rbind /$_fs "$INSTALL_DIR"/$_fs
		mount --make-rslave "$INSTALL_DIR"/$_fs
	done

	cp /etc/resolv.conf "$INSTALL_DIR"/etc/

	chroot "$INSTALL_DIR"/ /bin/bash "/opt/setup-void/setup-void-rootfs.sh"

	press_continue

	show_header "Chroot"
	echo -e "\n${GREEN}	Enter the rootfs via chroot to do things?\n"

	read -p "	(y/n): " _ENTER_ROOTF

    if [ "$_ENTER_ROOTF" = "y" ]; then
		echo -e "\nType ${NONE}exit${GREEN} and press ${NONE}Enter${GREEN} to exit rootfs.\n${NONE}"
		press_continue

		chroot "$INSTALL_DIR"/ /bin/bash
    fi

    unmount_rootfs
}


unmount_rootfs () {
	umount -R "$INSTALL_DIR/dev" "$INSTALL_DIR/proc" "$INSTALL_DIR/sys"
}


execute () {
	local _YN_CHOICE

	show_header "Install"
	echo -e "\n${GREEN}	This will install Void Linux in ${NONE}$INSTALL_DIR${GREEN}! ${NONE}\n"
	echo -e "\n${GREEN}	Continue? ${NONE}\n"

	read -p "	(y/n): " _YN_CHOICE

	if [ "$_YN_CHOICE" = "n" ]; then
		main_menu
	elif [ "$_YN_CHOICE" = "y" ]; then
		local _CHECK_PARAM
		_CHECK_PARAM=$(check_install_parameters)
		if  [ "$_CHECK_PARAM" = "ok" ]; then
			if ! [ -d "$INSTALL_DIR" ]; then
				mkdir -p "$INSTALL_DIR"
			fi

			if  [ "$DEV_OR_DIR" = "Device" ]; then
				PARTITION_ROOT=$(echo "$DEV_PART_ROOT" | grep -Eo '[0-9]+$')
				if [ "$FILESYSTEM" = "ext4" ]; then
					DEV_PART_BOOT=$DEV_PART_ROOT
					PARTITION_BOOT=$PARTITION_ROOT
				else
					PARTITION_BOOT=$(echo "$DEV_PART_BOOT" | grep -Eo '[0-9]+$')
				fi

				if [ "$ODROID_MODEL" = "C4" ]; then
					if ! [ "$STORAGE_TYPE" = "USB/SSD" ]; then
						BOOTLOADER_DEV=$INSTALL_DEV
						BOOTLOADER_PART=$DEV_PART_BOOT
						if ! [ "$FILESYSTEM" = "ext4" ]; then
							BOOTLOADER_MNT=$INSTALL_DIR"/boot"
						else
							BOOTLOADER_MNT=$INSTALL_DIR
						fi
					else
						BOOTLOADER_MNT=$INSTALL_DIR"/boot/ODROIDBIOS"
					fi
				fi

				prepare_storage
			else
				PARTITION_BOOT=$PARTITION_ROOT
				FILESYSTEM="ext4"
				if [ "$ODROID_MODEL" = "C4" ]; then
					BOOTLOADER_MNT=$INSTALL_DIR
				fi
			fi

			if echo "$TARGET_DEV" | grep  -e "mmcblk" > /dev/null || echo "$TARGET_DEV" | grep  -e "nvme" > /dev/null; then
				if ! echo "$PARTITION_ROOT" | grep  -e "p" > /dev/null; then
					PARTITION_ROOT="p"$PARTITION_ROOT
				fi
				if ! echo "$PARTITION_BOOT" | grep  -e "p" > /dev/null; then
					PARTITION_BOOT="p"$PARTITION_BOOT
				fi
			fi

			TARGET_DEV_MESS=$TARGET_DEV

			if ! [ "$ODROID_MODEL" = "M1" ]; then
				if [ "$TARGET_DEV" = "mmcblk0" ]; then
					TARGET_DEV_MESS="mmcblk1"
				elif [ "$TARGET_DEV" = "mmcblk1" ]; then
					TARGET_DEV_MESS="mmcblk0"
				fi
			fi

			install_rootfs
			if [ "$SHA_SUM" = "OK" ]; then
				parameters_to_file
				chroot_rootfs
				sync && sync && sync

				echo -e "\n${GREEN}	Installation successful.${NONE}\n"

				echo -e "\n${GREEN}	Do you want to pack the new rootfs into a .tar.xz file?${NONE}\n"

				_YN_CHOICE=""
				read -p "	(y/n): " _YN_CHOICE

				if [ "$_YN_CHOICE" = "y" ]; then
					pack_rootfs
				fi

				if [ "$DEV_OR_DIR" = "Device" ]; then
					echo -e "\n${GREEN}	Do you want to unmount the storage device?${NONE}\n"

					_YN_CHOICE=""
					read -p "	(y/n): " _YN_CHOICE

					if [ "$_YN_CHOICE" = "y" ]; then
						sync
						umount -R "$INSTALL_DIR"
					fi
				fi
				SHA_SUM=""
			fi

		else
			echo -e "\n${GREEN}	Aborted! One or more parameters are not set.${NONE}\n"
			press_continue
		fi
	fi
}


help_me () {
	show_header "Help"

	echo -e "\n${GREEN}	You stuck in a submenu and don't know what to type?${NONE}\n"
	echo -e "${GREEN}	Leave it blank and press enter.${NONE}"
	echo -e "${GREEN}	That will bring you back to main menu.${NONE}\n\n"

	echo -e "\n${GREEN}	You have a idea or found a bug?${NONE}\n"
	echo -e "${GREEN}	Open a issue on${NONE}\n"
	echo -e "	https://gitlab.com/frankelectron1/setup-void\n"
	press_continue
}


main_menu () {
	local _MM_CHOICE

	show_header "Main Menu"
	echo -e "\n${GREEN}	Type a character and press Enter to change a parameter then install with i.${NONE}\n"
	echo -e "\n	1 ${GREEN}Odroid Model${NONE}						[ $ODROID_MODEL ]\n"
	echo -e "	2 ${GREEN}Storage device Type${NONE}					[ $STORAGE_TYPE ]\n"
	echo -e "	3 ${GREEN}Install onto device or into directory${NONE}			[ $DEV_OR_DIR ]\n"

	if [ "$DEV_OR_DIR" = "Device" ]; then
		echo -e "		a ${GREEN}Filesystem${NONE}					[ $FILESYSTEM ]\n"
		echo -e "		b ${GREEN}Device for / (rootfs)${NONE}				[ $INSTALL_DEV ]\n"
		echo -e "		c ${GREEN}Partition for / (rootfs)${NONE}			[ $DEV_PART_ROOT ]\n"

		if [ "$FILESYSTEM" = "btrfs" ] || [ "$FILESYSTEM" = "xfs" ]; then
			echo -e "		d ${GREEN}Partition for /boot${NONE}				[ $DEV_PART_BOOT ]\n"
		fi

		echo -e "		e ${GREEN}Mountpoint for / (rootfs)${NONE}			[ $INSTALL_DIR ]\n"

		if [ "$ODROID_MODEL" = "C4" ] && [ "$STORAGE_TYPE" = "USB/SSD" ]; then
			echo -e "		f ${GREEN}Device for C4 Petiteboot${NONE}			[ $BOOTLOADER_DEV ]\n"
			echo -e "		g ${GREEN}Partition for C4 Petiteboot${NONE}			[ $BOOTLOADER_PART ]\n"
		fi

	elif [ "$DEV_OR_DIR" = "Directory" ]; then
		echo -e "		a ${GREEN}Directory${NONE}					[ $INSTALL_DIR ]\n"
		echo -e "		b ${GREEN}Target partition number${NONE}			[ $PARTITION_ROOT ]\n"
	fi

	echo -e "\n	i ${GREEN}Install${NONE}\n"
	echo -e "	p ${GREEN}Pack rootfs into .tar.xz${NONE}\n"
	echo -e "	u ${GREEN}Unmount device${NONE}\n"
	echo -e "	w ${GREEN}Wipe storage device${NONE}\n"
	echo -e "	n ${GREEN}New partition(s)    (cfdisk)${NONE}\n"
	echo -e "	h ${GREEN}Help${NONE}\n"
	echo -e "\n	x ${GREEN}Exit${NONE}\n"

	read -p "	 : " _MM_CHOICE

	if [ "$_MM_CHOICE" = "x" ]; then
		exit_script

	elif [ "$_MM_CHOICE" = "1" ]; then
		odroid_model

	elif [ "$_MM_CHOICE" = "2" ]; then
		storage_type

	elif [ "$_MM_CHOICE" = "3" ]; then
		if [ "$DEV_OR_DIR" = "	" ] || [ "$DEV_OR_DIR" = "Directory" ]; then
			DEV_OR_DIR="Device"
		else
			DEV_OR_DIR="Directory"
		fi

	elif [ "$_MM_CHOICE" = "a" ] && [ "$DEV_OR_DIR" = "Device" ]; then
		filesystem_rootfs

	elif [ "$_MM_CHOICE" = "b" ] && [ "$DEV_OR_DIR" = "Device" ]; then
		install_dev

	elif [ "$_MM_CHOICE" = "c" ] && [ "$DEV_OR_DIR" = "Device" ]; then
		root_partition

	elif [ "$_MM_CHOICE" = "d" ] && [ "$DEV_OR_DIR" = "Device" ]; then
		boot_partition

	elif [ "$_MM_CHOICE" = "e" ] && [ "$DEV_OR_DIR" = "Device" ]; then
		install_dir

	elif [ "$_MM_CHOICE" = "f" ] && [ "$DEV_OR_DIR" = "Device" ]; then
		bootloader_dev

	elif [ "$_MM_CHOICE" = "g" ] && [ "$DEV_OR_DIR" = "Device" ]; then
		bootloader_part

	elif [ "$_MM_CHOICE" = "a" ] && [ "$DEV_OR_DIR" = "Directory" ]; then
		install_dir

	elif [ "$_MM_CHOICE" = "b" ] && [ "$DEV_OR_DIR" = "Directory" ]; then
		target_part

	elif [ "$_MM_CHOICE" = "n" ]; then
		new_partition

	elif [ "$_MM_CHOICE" = "w" ]; then
		wipe_storage

	elif [ "$_MM_CHOICE" = "i" ]; then
		execute

	elif [ "$_MM_CHOICE" = "p" ]; then
		pack_rootfs

	elif [ "$_MM_CHOICE" = "u" ]; then
		unmount_device

	elif [ "$_MM_CHOICE" = "h" ]; then
		help_me

	fi
}


if [ "$(id -u)" -ne 0 ]; then
	echo -e "void-odroid-installer.sh needs to run as root"
	exit
fi

wellcome
check_dependencies

while true; do
	main_menu
done
