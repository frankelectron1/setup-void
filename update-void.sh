#!/bin/bash
#set -e

GREEN=$(tput setaf 2)
NONE=$(tput sgr0)
SCRIPT_NAME=$(basename "$0")


show_usage () {
	echo -e "\nupdate-void - written by FrankElectron\n"
	echo -e "usage:\n"
	echo -e "    -i		Install update-void into /usr/local/bin\n"
	echo -e "    -r		Remove update-void from /usr/local/bin\n"
	echo -e "\n    -u		Run	xbps-remove -Oy (optional)"
	echo -e "    			xbps-install -S"
	echo -e "    			xbps-install -uy xbps"
	echo -e "    			xbps-install -u \n"
}


install_update_void () {
	echo -e "\n${GREEN}Installing update-void into /usr/local/bin?${NONE}\n"
	read -p "    (y/n): " _YN_CHOICE

	if [ "$_YN_CHOICE" = "y" ]; then
		cp ./"${SCRIPT_NAME}" /usr/local/bin/update-void
		chmod +x /usr/local/bin/update-void
		echo -e "\n${GREEN}Installing successful.${NONE}\n"
	fi

	exit
}


remove_update_void () {
	if [ -f /usr/local/bin/update-void ]; then
		echo -e "\n${GREEN}Removing update-void?${NONE}\n"
		read -p "    (y/n): " _YN_CHOICE

		if [ "$_YN_CHOICE" = "y" ]; then
			rm /usr/local/bin/update-void
			echo -e "\n${GREEN}Removing successful.${NONE}\n"
		fi
	else
		echo -e "\n${GREEN}update-void is not installed!${NONE}\n"
	fi

	exit
}


cleaning_cache () {
	echo -e "\n${GREEN}Cleaning XBPS-Cache?${NONE}\n"
	read -p "    (y/n): " _YN_CHOICE

	if [ "$_YN_CHOICE" = "y" ]; then
		xbps-remove -Oy #| sed 's/^/    /'
	fi
}


update_packages () {
	echo -e "\n${GREEN}Updateing Source.${NONE}\n"
	xbps-install -S #| sed 's/^/    /'

	echo -e "\n${GREEN}Updateing XBPS.${NONE}\n"
	xbps-install -uy xbps #| sed 's/^/    /'

	echo -e "\n${GREEN}Updateing Packages.${NONE}\n"
	xbps-install -u #| sed 's/^/    /'

	echo -e "\n${GREEN}Updateing successful.${NONE}\n"
}



if [ "$1" = "-i" ]; then
	install_update_void
elif [ "$1" = "-r" ]; then
	remove_update_void
elif [ "$1" = "-u" ]; then
	cleaning_cache
	update_packages
	exit
else
	show_usage
	exit
fi

